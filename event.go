package main

import "fmt"

//import "fmt"

type Event struct {
	ID int
	User int
	Business string
	Budget int
	StartTime string
	EndTime string
	Location string
	Description string
}

func InsertEvent(event *Event) error {
	var id int
	err := db.QueryRow(`
		INSERT INTO "events"("user", "business", "budget", "start_time", "end_time","location", "description") VALUES ($1, $2, $3, $4, $5, $6, $7)
		RETURNING id`,
			event.User, event.Business, event.Budget, event.StartTime, event.EndTime,
				event.Location, event.Description).Scan(&id)
	if err != nil {
		return err
	}
	event.ID = id
	return nil
}


func GetEventByID(id int) (*Event, error) {
	var (
		budget, user int
		business, startTime, endTime, location, description, test string
	)

	user = 1
	err := db.QueryRow(`SELECT user,user_id, business, budget, start_time,
		end_time, location, description
		FROM events WHERE id=$1`,id).Scan(&test, &user ,&business, &budget, &startTime, &endTime, &location, &description)

	if err != nil {
		return nil, err
	}

	return &Event{
		ID: id,
		User: user,
		Business: business,
		Budget: budget,
		StartTime: startTime,
		EndTime: endTime,
		Location: location,
		Description: description,
	}, nil
}

func GetListEvents(id int) ([]*Event, error) {
	rows, err := db.Query(`
		SELECT id, business, budget, start_time,
		end_time, location, description, user_id
		FROM events
		ORDER BY id DESC
		limit 4
	`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var events    = []*Event{}
	var (

		cid, budget, user_id int
		location, description, start_time, end_time, business string
	)
	for rows.Next() {
		if err = rows.Scan(&cid, &business, &budget, &start_time, &end_time, &location, &description, &user_id); err != nil {
			return nil, err
		}
		events = append(events, &Event{
			ID:     cid,
			User: user_id,
			Business: business,
			Budget: budget,
			StartTime:  start_time,
			EndTime:   end_time,
			Location: location,
			Description: description,
		})

	}
	fmt.Println(id)

	//for j:=0;j<len(events) ;j++  {
	//	fmt.Println(events[j])
	//}

	return events, nil
}

