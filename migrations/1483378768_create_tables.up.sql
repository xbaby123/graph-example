DROP TABLE IF EXISTS "business";
CREATE TABLE "business" (
  "id" int2 NOT NULL DEFAULT nextval('business_id_seq'::regclass),
  "title" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "description" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "business" OWNER TO "ironsight";

-- ----------------------------
-- Records of business
-- ----------------------------
BEGIN;
INSERT INTO "business" VALUES (1, 'IT', 'Information Technology');
INSERT INTO "business" VALUES (2, 'Freelance', 'Hiring Freelancer');
INSERT INTO "business" VALUES (3, 'Rent', 'Reting apartment');
INSERT INTO "business" VALUES (4, 'Elelectric', 'Making stuff');
COMMIT;

-- ----------------------------
-- Primary Key structure for table business
-- ----------------------------
ALTER TABLE "business" ADD CONSTRAINT "business_pkey" PRIMARY KEY ("id");


DROP TABLE IF EXISTS "events";
CREATE TABLE "events" (
  "user" int8 DEFAULT NULL,
  "business" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "budget" int8 DEFAULT NULL,
  "start_time" timestamp(0) DEFAULT NULL,
  "end_time" timestamp(0) DEFAULT NULL,
  "location" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "description" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "id" int8 NOT NULL DEFAULT nextval('events_id_seq'::regclass),
  "user_id" int8 DEFAULT NULL
)
;
ALTER TABLE "events" OWNER TO "ironsight";

-- ----------------------------
-- Records of events
-- ----------------------------
BEGIN;
INSERT INTO "events" VALUES (1, 'IT', 123200, '2018-11-22 10:27:18', '2018-11-22 10:27:18', 'HCM city', 'Some demo event in HCM city about IT', 4, 1);
INSERT INTO "events" VALUES (1, 'IT', 123200, '2018-11-22 10:27:18', '2018-11-22 10:27:18', 'HCM city', 'Some demo event in HCM city about IT', 5, 1);
INSERT INTO "events" VALUES (2, 'IT', 123200, '2018-11-22 10:27:18', '2018-11-22 10:27:18', 'HCM city', 'Some demo event in HCM city about IT', 6, 2);
INSERT INTO "events" VALUES (2, 'Rental', 800, '2018-04-25 10:27:12', '2018-11-22 10:27:18', 'Hà Nội', 'This is a event about build a building for renting', 3, 2);
INSERT INTO "events" VALUES (2, 'Rental', 800, '2018-04-25 10:27:12', '2018-11-22 10:27:18', 'Hà Nội', 'This is a event about build a building for renting', 2, 2);
INSERT INTO "events" VALUES (1, 'IT11', 1000, '2018-05-16 10:25:51', '2018-09-13 10:25:58', 'TP Hồ Chí Minh', 'Some event in HCM About Some Tech', 1, 1);
COMMIT;

-- ----------------------------
-- Primary Key structure for table events
-- ----------------------------
ALTER TABLE "events" ADD CONSTRAINT "events_pkey" PRIMARY KEY ("id");

DROP TABLE IF EXISTS "user_business";
CREATE TABLE "user_business" (
  "user_id" int8 NOT NULL DEFAULT NULL,
  "business_id" int8 NOT NULL DEFAULT NULL,
  "description" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "user_business" OWNER TO "ironsight";

-- ----------------------------
-- Records of user_business
-- ----------------------------
BEGIN;
INSERT INTO "user_business" VALUES (1, 1, NULL);
INSERT INTO "user_business" VALUES (1, 2, NULL);
INSERT INTO "user_business" VALUES (1, 4, NULL);
INSERT INTO "user_business" VALUES (2, 2, NULL);
INSERT INTO "user_business" VALUES (2, 3, NULL);
INSERT INTO "user_business" VALUES (2, 4, NULL);
COMMIT;


DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "email" varchar(100) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "username" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "company" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "users" OWNER TO "ironsight";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "users" VALUES (1, '1@x.co', 'mike123', 'Mike', 'MilTech');
INSERT INTO "users" VALUES (2, 'test@gmail.co', 'alex45', 'Alex', 'SF Corp');
COMMIT;

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");