package main

import "fmt"

type Business struct {
	ID int
	Title string
	Description string
}

func getListBusiness(user_id int) ([]*Business, error) {
	rows, err := db.Query(`
		SELECT b.title, b.description
		FROM user_business u
		LEFT JOIN business b ON b.id = u.business_id
		WHERE u.user_id = $1

	`, user_id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var businessList    = []*Business{}
	var (
		description, title string
	)
	for rows.Next() {
		if err = rows.Scan(&title, &description); err != nil {
			return nil, err
		}
		businessList = append(businessList, &Business{
			Title: title,
			Description: description,
		})

	}

	for j:=0;j<len(businessList) ;j++  {
		fmt.Println(businessList[j])
	}

	return businessList, nil
}

