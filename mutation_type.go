package main

import (
	"github.com/graphql-go/graphql"
	"strconv"
)




var EventMutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "EventMutation",
	Fields: graphql.Fields{
		"createEvent": &graphql.Field{
			Type: EventType,
			Args: graphql.FieldConfigArgument{
				"user": &graphql.ArgumentConfig{
					Description: "New Event User",
					Type:graphql.NewNonNull(graphql.Int),
				},
				"business": &graphql.ArgumentConfig{
					Description: "New Event Business",
					Type:graphql.NewNonNull(graphql.String),
				},
				"budget": &graphql.ArgumentConfig{
					Description: "New Event User",
					Type:graphql.NewNonNull(graphql.Int),
				},
				"start_time": &graphql.ArgumentConfig{
					Description: "New Event Start Date",
					Type:graphql.NewNonNull(graphql.String),
				},
				"end_time": &graphql.ArgumentConfig{
					Description: "New Event End Date",
					Type:graphql.NewNonNull(graphql.String),
				},
				"location": &graphql.ArgumentConfig{
					Description: "New Event Location",
					Type:graphql.NewNonNull(graphql.String),
				},
				"description": &graphql.ArgumentConfig{
					Description: "New Event Description",
					Type:graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				user := p.Args["user"].(int)
				business := p.Args["business"].(string)
				budget := p.Args["budget"].(int)
				startTime := p.Args["start_time"].(string)
				endTime := p.Args["end_time"].(string)
				location := p.Args["location"].(string)
				description := p.Args["description"].(string)


				event := &Event{
					User: user,
					Business: business,
					Budget: budget,
					StartTime: startTime,
					EndTime: endTime,
					Location: location,
					Description: description,
				}
				err := InsertEvent(event)
				return event, err
			},
		},
	},
})

