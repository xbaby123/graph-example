#!/bin/bash
set -e
cd "$(dirname "$0")"/
migrate -database postgres://ironsight@localhost:5432/graphql -path ./migrations $@
