package main

import (
	"github.com/graphql-go/graphql"
	"strconv"
)



var EventQueryType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Query",
	Fields: graphql.Fields{
		"event": &graphql.Field{
			Type: graphql.NewList(EventType),
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Description: "Event ID",
					Type: graphql.ID,
					DefaultValue: "0",
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				i := p.Args["id"].(string)
				id, err := strconv.Atoi(i)
				if err != nil {
					return nil, err
				}
				if id != 0 {
					event, nil := GetEventByID(id)
					var events    = []*Event{}
					events = append(events, event)
					return events, nil
				} else {
					return GetListEvents(0)
				}
			},
		},
	},
})
