# graphql-go-example

Example GraphQL API implemented in Go and backed by Postgresql

## How to run it

To run this project you need to:
- install golang, see [this guide](https://golang.org/doc/install)
- install postgresql (for ubuntu follow [this guide](https://help.ubuntu.com/community/PostgreSQL)). For this application, I created a postgresql user called `vagrant` with `vagrant` as password and the database called `graphql`, but of course you can change these settings in `./migrate.sh` and in `./main.go` files.

## Commands

This application exposes a single endpoints `/graphql` which accepts both mutations and queries.
The following are examples of curl calls to this endpoint:

```bash
http :8080/graphql <<< '{event(id:1){ budget, start_time, end_time, location, description}}'

http :8080/graphql <<< '{event(id:1){ budget, start_time, end_time, location, description, user{ name, username, email , company}}}'

http :8080/graphql <<< '{event(id:1){ budget, start_time, end_time, location, description, user{ name, username, email , company, business{title, description}}}}'

http :8080/graphql <<< '{event{ budget, start_time, end_time, location, description, user{ name, username, email , company, business{title, description}}}}'
```
