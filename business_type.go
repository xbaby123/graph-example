package main

import (
	"github.com/graphql-go/graphql"
)

var BusinessType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Business",
	Fields: graphql.Fields{
		"title": &graphql.Field {
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if business, ok := p.Source.(*Business); ok == true {
					return business.Title, nil
				}
				return nil, nil
			},
		},
		"description": &graphql.Field{
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if business, ok := p.Source.(*Business); ok == true {
					return business.Description, nil
				}
				return nil, nil
			},
		},
	},
})

