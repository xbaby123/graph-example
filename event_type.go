package main

import (
	"github.com/graphql-go/graphql"
)

var EventType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Event",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.NewNonNull(graphql.ID),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.ID, nil
				}
				return nil, nil
			},
		},
		"budget": &graphql.Field{
			Type: graphql.NewNonNull(graphql.Int),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.Budget, nil
				}
				return nil, nil
			},
		},
		"start_time": &graphql.Field{
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.StartTime, nil
				}
				return nil, nil
			},
		},
		"end_time": &graphql.Field{
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.EndTime, nil
				}
				return nil, nil
			},
		},
		"location": &graphql.Field{
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.Location, nil
				}
				return nil, nil
			},
		},
		"description": &graphql.Field{
			Type: graphql.NewNonNull(graphql.String),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if eventObject, ok := p.Source.(*Event); ok == true {
					return eventObject.Description, nil
				}
				return nil, nil
			},
		},
	},
})

func init() {
	EventType.AddFieldConfig("user", &graphql.Field{
		Type: graphql.NewNonNull(UserType),
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			if event, ok := p.Source.(*Event); ok == true {
				return GetUserByID(event.User)
			}
			return nil, nil
		},
	})
}
